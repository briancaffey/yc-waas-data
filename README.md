# YC Work at a Startup data

This repo will document my processing for collecting and analyzing YC's Work at a Startup data.


## Scraping data

I'll use playwright to scrape data from [https://www.workatastartup.com/](https://www.workatastartup.com/).

We can generate the python script for logging in and scraping data with:

```
python -m playwright codegen
```

This will open a new window in which we can navigate to the page with the data we want to collect. Our browsing actions will be recorded and sent to STDOUT.

This will scaffold the important parts of the script that we can use to access the data:

I've added `os` to keep my login credentials out of the code:

```py
import os
from playwright import sync_playwright

def run(playwright):
    browser = playwright.chromium.launch(headless=False)
    context = browser.newContext()

    # Open new page
    page = context.newPage()

    # Go to https://www.workatastartup.com/
    page.goto("https://www.workatastartup.com/")

    # Click text="Log In ›"
    page.click("text=\"Log In ›\"")
    # assert page.url == "https://account.ycombinator.com/?continue=https://www.workatastartup.com/application"

    # Fill //div[normalize-space(.)='Username']/div[1]/input[normalize-space(@type)='text']
    page.fill("//div[normalize-space(.)='Username']/div[1]/input[normalize-space(@type)='text']", os.environ.get("WAAS_USERNAME"))

    # Press Tab
    page.press("//div[normalize-space(.)='Username']/div[1]/input[normalize-space(@type)='text']", "Tab")

    # Fill input[type="password"]
    page.fill("input[type=\"password\"]", os.environ.get("WAAS_PASSWORD"))

    # Click text="Log in"
    # with page.expect_navigation(url="https://www.workatastartup.com/application/personal"):
    with page.expect_navigation():
        page.click("text=\"Log in\"")

    # Click text="Companies"
    # with page.expect_navigation(url="https://www.workatastartup.com/companies?companySize=any&expo=any&industry=any&layout=list-compact&matchingJobs=any&remote=any&sortBy=name"):
    with page.expect_navigation():
        page.click("text=\"Companies\"")

    # Check input[name="layout"]
    page.check("input[name=\"layout\"]")
    # assert page.url == "https://www.workatastartup.com/companies?companySize=any&expo=any&industry=any&layout=list&matchingJobs=any&remote=any&sortBy=name"

    # Click //div[normalize-space(.)='Software Engineer, Backend']/i[normalize-space(@title)='toggle' and normalize-space(@role)='button']
    page.click("//div[normalize-space(.)='Software Engineer, Backend']/i[normalize-space(@title)='toggle' and normalize-space(@role)='button']")

    # Click //div[normalize-space(.)='Software Engineer, Full Stack']/i[normalize-space(@title)='toggle' and normalize-space(@role)='button']
    page.click("//div[normalize-space(.)='Software Engineer, Full Stack']/i[normalize-space(@title)='toggle' and normalize-space(@role)='button']")

    # Close page
    page.close()

    # ---------------------
    context.close()
    browser.close()

with sync_playwright() as playwright:
    run(playwright)
```

## Jupyter notebook

[https://ipython.readthedocs.io/en/stable/install/kernel_install.html#kernels-for-different-environments](https://ipython.readthedocs.io/en/stable/install/kernel_install.html#kernels-for-different-environments)

```
python -m ipykernel install --user --name myenv --display-name "Python (myenv)"
```

## JS Scraping

First, we want to select "Show full details", so either click that checkbox or do:

```
const toggleDetails = document.getElementsByClassName("checkbox-inline")[0]

toggleDetails.click()
```

Next, run this until it gets to the end

```
window.scrollTo(0,document.body.scrollHeight);
```

Then we need to expand each job listing:

```
const jobs = document.getElementsByClassName("job-name")
```

```
for (let job of jobs) {
    job.click()
}
```

## Word Clouds for Descriptions of Companies, Founders, Job Titles and Job Descriptions

## Charting libraries?

##